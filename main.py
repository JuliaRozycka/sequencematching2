import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib.colors import LinearSegmentedColormap
from enum import Enum


def is_protein(sequence):
    """
    Function used to check if protein sequence is correct.

    Parameters:
        sequence (str): A string of protein sequence.

    Returns:
        boolean: True if sequence consist from only proteins

    """
    list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
            'X', 'Y', 'Z']
    for i in sequence:
        if i not in list:
            return False
    return True



def is_dna_sequence(sequence):
    """
    Function used to check if DNA sequence is correct.

    Parameters:
        sequence (str): A string of DNA sequence.

    Returns:
        boolean: True if the sequence consist from only nucelotides

    """
    list = ['A', 'C', 'T', 'G', 'a', 'c', 't', 'g']
    for i in sequence:
        if i not in list:
            return False
    return True


class InputType(Enum):  # enum class used for picking the input type
    FILE = "file"
    PLAIN_TEXT = "plain_text"


class DataType(Enum):  # enum class used for picking the type of data - protein or DNA
    PROTEIN = "protein"
    DNA = "dna"


def Needelman_Wunsch(seq1, seq2, data_type, gap=-1, match=1, missmatch=-2, seq1_input_type=InputType.PLAIN_TEXT, seq2_input_type=InputType.PLAIN_TEXT):
    """
    Function generates dotplot matrix.

    Parameters:
        seq1 (str): first sequence
        seq2 (str): second sequence
        gap (int): DNA or protein
        data_type (Enum): data type
        missmatch (int): default is from plain text
        match (int): default is from plain text

    Returns:
        A (np.array): matrix - the dotplot of two sequences
    """
    seq1 = read_sequence(seq1_input_type, data_type, seq1)
    seq2 = read_sequence(seq2_input_type, data_type, seq2)

    cmap1 = LinearSegmentedColormap.from_list('rg', ["w", "magenta", "#000000"], N=256)
    A = np.zeros([len(seq1) + 1, len(seq2) + 1])
    B = np.copy(A)
    B = np.array(B, dtype=object)
    C = np.copy(A)

    A[:, 0] = np.linspace(0, -len(seq1), len(seq1) + 1)
    A[0, :] = np.linspace(0, -len(seq2), len(seq2) + 1)
    for i in range(1, len(seq2) + 1):
        B[0][i] = 'L'
    for i in range(1, len(seq1) + 1):
        B[i][0] = 'U'

    seq1 = "-" + seq1
    seq2 = "-" + seq2
    for i in range(1, len(seq1)):
        for j in range(1, len(seq2)):
            if (seq1[i] == seq2[j]):
                D = A[i - 1][j - 1] + match
            else:
                D = A[i - 1][j - 1] + missmatch

            L = A[i, j - 1] + gap
            U = A[i - 1, j] + gap
            A[i][j] = max(D, L, U)
            if A[i][j] == D:
                B[i][j] = 'D'
            elif A[i][j] == L:
                B[i][j] = 'L'
            else:
                B[i][j] = 'U'
    B[0][0] = "0"
    w = 1
    i = 1
    j = 1
    s1 = ""
    s2 = ""
    while w != "0":
        w = B[len(seq1) - i][len(seq2) - j]
        if w == "D":
            i = i + 1
            s1 += seq1[len(seq1) - i + 1]
            j = j + 1
            s2 += seq2[len(seq2) - j + 1]
            C[len(seq1) - i + 1][len(seq2) - j + 1] = 1
        elif w == "U":
            i = i + 1
            s2 += "-"
            s1 += seq1[len(seq1) - i + 1]
            C[len(seq1) - i + 1][len(seq2) - j] = 1

        elif w == "L":
            j = j + 1
            s2 += seq2[len(seq2) - j + 1]
            s1 += "-"
            C[len(seq1) - i][len(seq2) - j + 1] = 1
        C[0][0] = 1

    missmatch_prc = round((s1.count("-") + s2.count("-")) / len(s1) * 100, 2)
    match_prc = 100 - missmatch_prc
    #path = "   C:/Users/User/Desktop/SHIT/dna_seq_1.fasta"


    return A, seq1, seq2, B, s1[::-1], s2[::-1], C, match, missmatch, gap, match_prc, missmatch_prc


def smith_waterman(sequence1, sequence2, data_type, match=1, missmatch=-2, gap=-1, seq1_input_type=InputType.PLAIN_TEXT,
                   seq2_input_type=InputType.PLAIN_TEXT):
    sequence1 = read_sequence(seq1_input_type, data_type, sequence1)
    sequence2 = read_sequence(seq2_input_type, data_type, sequence2)

    sequence1 = "-" + sequence1
    sequence2 = "-" + sequence2
    A = np.zeros([len(sequence1) + 1, len(sequence2) + 1])
    B = np.copy(A)
    B = np.array(B, dtype=object)
    C = np.copy(A)
    A[:, 0] = np.linspace(0, 0, len(sequence1) + 1)
    A[0, :] = np.linspace(0, 0, len(sequence2) + 1)
    for i in range(1, len(sequence2) + 1):
        B[0][i] = '0'
    for i in range(1, len(sequence1) + 1):
        B[i][0] = '0'

    B[0][0] = "0"
    maks = 0
    imaks = 0
    jmaks = 0
    for i in range(1, len(sequence1)):
        for j in range(1, len(sequence2)):
            if sequence1[i] == sequence2[j]:
                D = A[i - 1][j - 1] + match
            else:
                D = A[i - 1][j - 1] + missmatch
            U = A[i - 1][j] + gap
            L = A[i][j - 1] + gap
            if max(D, U, L) > maks:
                maks = max(D, U, L)
                imaks = i
                jmaks = j
            if max(D, U, L) == D:
                if D <= 0:
                    D = 0
                    B[i][j] = "0"
                else:
                    B[i][j] = "D"
                A[i][j] = D
            elif max(D, U, L) == U:
                if U <= 0:
                    U = 0
                    B[i][j] = "0"
                else:
                    B[i][j] = "U"
                A[i][j] = U
            else:
                if L <= 0:
                    L = 0
                    B[i][j] = "0"
                else:
                    B[i][j] = "L"
                A[i][j] = L

    w = 1
    i = 0
    j = 0
    seq1 = ""
    seq2 = ""
    C[imaks][jmaks] = 1
    while w != "0":
        w = B[imaks - i][jmaks - j]
        if w == "D":
            i += 1
            j += 1
            seq1 += sequence1[imaks - i + 1]
            seq2 += sequence2[jmaks - j + 1]
        elif w == "U":
            i += 1
            seq1 += sequence1[imaks - i + 1]
            seq2 += "-"
        elif w == "L":
            j += 1
            seq1 += "-"
            seq2 += sequence2[jmaks - j + 1]
        C[imaks - i][jmaks - j] = 1

    count = 0
    for i in range(len(seq1)):
        if seq1[i] == seq2[i]:
            count += 1
    match_prc = round(count / len(seq1) * 100, 2)
    missmatch_prc = round((len(seq1) - count) / len(seq1) * 100, 2)
    breaks_prc = round((seq1.count("-") + seq2.count("-")) / len(seq1) * 100, 2)

    return A, sequence1, sequence2, B, seq1[::-1], seq2[
                                                   ::-1], C, match, missmatch, gap, match_prc, missmatch_prc, breaks_prc, imaks-i, jmaks-j


def convert(string):
    """
    Function used to convert sequence into a list of nucleotides.
    Parameters:
        string (str): sequence

    Returns:
        list (list): list of single letters
    """
    list = []
    list[:0] = string
    return list


def create_heatmap(data, seq1_title, seq2_title,title):
    """
    Additional function that creates a heatmap from dotplot.

    Parameters:
        data (np.array): The numpy array of 0 and 1
        seq1_title (str): title of y-axis
        seq2_title (str): title od x-axis

    Returns:
        fig (figure): A matplotlib pyplot figure
    """
    cmap1 = LinearSegmentedColormap.from_list('rg', ["w", "magenta", "#000000"], N=256)
    cmap2 = LinearSegmentedColormap.from_list('rg', ["w", "magenta"], N=256)

    match = data[7]
    missmatch = data[8]
    gap = data[9]

    list1 = convert(data[1])
    list2 = convert(data[2])

    plt.subplot(1, 2, 1)
    hmap = sns.heatmap(data[0], cmap=cmap1, cbar=False, linewidths=0.5, linecolor="black", annot=True)
    for _, spine in hmap.spines.items():
        spine.set_visible(True)
    plt.suptitle(title, fontsize=20, y=1)
    plt.title("match = " + str(match) + "   missmatch = " + str(missmatch) + "   gap = " + str(gap))
    plt.ylabel(seq1_title, fontsize=15)
    plt.xlabel(seq2_title, fontsize=15)
    plt.yticks(np.arange(0.5, len(data[1]) + 0.5, step=1), list1, rotation=0)
    plt.xticks(np.arange(0.5, len(data[2]) + 0.5, step=1), list2)

    plt.subplot(1, 2, 2)
    hmap = sns.heatmap(data[6], cmap=cmap2, cbar=False, linewidths=0.5, linecolor="black")
    for _, spine in hmap.spines.items():
        spine.set_visible(True)
    plt.ylabel(seq1_title, fontsize=15)
    plt.xlabel(seq2_title, fontsize=15)
    plt.yticks(np.arange(0.5, len(data[1]) + 0.5, step=1), list1, rotation=0)
    plt.xticks(np.arange(0.5, len(data[2]) + 0.5, step=1), list2)

    fig = plt.gcf()
    fig.set_size_inches(14, 6)
    plt.close()

    return fig


def save_plot(figure, name):
    """
    Function saves pictue.

    Parameteres:
        figure (figure): pyplot figure
        name (str): name of file
    """
    figure.savefig(name, dpi=100, bbox_inches='tight')



# save_plot(create_heatmap(N_D_road("ACTGTA","ACTTGTA"),"Sequence 1","Sequence 2"),'deh_gene.jpg')
def read_file(filename):
    """
    Function for reading FASTA files.

    Parameters:
        filename (str): Name of a fasta file.

    Returns:
        joines (str): The sequence from that file.
    """
    x = open(filename, "r")
    lines = x.readlines()[1:]
    joined = ""
    for n in range(len(lines)):
        joined += lines[n].replace("\n", "")
    return str(joined)


def read_sequence(input_type, data_type, text_or_file_path):
    """
    Function used to read sequence from file or from plain text.
    Returns sequence. Default value of input_type is plain_text.

    Parameters:
        input_type (enum): Enum input type - plain  text or from file
        data_type (enum): Enum data type - DNA or protein
        text_or_file_path: Sequence in text or file path to file

    Returns:
        str: DNA or protein sequence
    """
    sequence = ""

    if input_type.value == "file":
        sequence = read_file(text_or_file_path)
    elif input_type.value == "plain_text":
        sequence = text_or_file_path.upper()

    if data_type.value == "protein":
        if is_protein(sequence) is False:
            raise TypeError('your protein sequence is wrong')
    elif data_type.value == "dna":
        if is_dna_sequence(sequence) is False:
            raise TypeError('your DNA sequence is wrong')

    return sequence

def save_data_to_file(data, filename):
    lines = [data[4], data[5],data[10],data[11]]
    with open(filename, 'w') as f:
        f.write("Sequence 1\tSequence 2\tMatch %\tMissmatch %")
        f.write('\n')

        for line in lines:
            f.write(str(line))
            f.write('\t')

def interactive_program():
    print("Hi! Welcome to QASP - Quantitative Adjustment of the Sequence Pairs")
    name = str(input("To get you started enter your name: "))
    print(f"Nice to meet you {name}. Let's begin!")
    sequence1 = ""
    sequence2 = ""
    while True:
        data_type = str(input("Will be those sequences:\nA)DNA\nB)protein?\n"))
        if data_type == "A":
            data_type = DataType.DNA
            break
        elif data_type == "B":
            data_type = DataType.PROTEIN
            break
        elif data_type not in ("A", "B"):
            print("Incorrect answer. Choose option A or B.")
            continue
        else:
            break
    while True:
        input_type = str(input("What kind of input is the first sequence:\nA)string\nB)file\n"))
        if input_type == "A":
            input_type1 = InputType.PLAIN_TEXT
            sequence1 = str(input("Enter the first sequence: "))
            if data_type is DataType.PROTEIN:
                if is_protein(sequence1) is False:
                    print("wrong sequence")
                    continue
                else:
                    break
            elif data_type is DataType.DNA:
                if is_dna_sequence(sequence1) is False:
                    print("wrong sequence")
                    continue
                else:
                    break
        elif input_type == "B":
            sequence1 = str(input("Enter file path: ")).strip()
            input_type1 = InputType.FILE
            break
        elif input_type not in ("A", "B"):
            print("Incorrect answer. Choose option A or B.")
            continue
        else:
            break
    while True:
        input_type = str(input("What kind of input is the second sequence:\nA)string\nB)file\n"))
        if input_type == "A":
            sequence2 = str(input("Enter the second sequence: "))
            input_type2 = InputType.PLAIN_TEXT
            if data_type is DataType.PROTEIN:
                if is_protein(sequence2) is False:
                    print("wrong sequence")
                    continue
                else:
                    break
            elif data_type is DataType.DNA:
                if is_dna_sequence(sequence2) is False:
                    print("wrong sequence")
                    continue
                else:
                    break
        elif input_type == "B":
            sequence2 = str(input("Enter file path: ")).strip()
            input_type2 = InputType.FILE
            break
        elif input_type not in ("A", "B"):
            print("Incorrect answer. Choose option A or B.")
            continue
        else:
            break
    while True:
        method = str(input("What method would you like to use?\nA)Smith-Waterman\nB)Needleman-Wunsch\n"))
        if method == "A":
            gmm = str(input("Current values of match/missmatch/gap are 1/-2/-1. Would you like to change it? (Y/n)"))
            if gmm == "Y":
                print("Values should be integer. Missmatch and gap should be negative.")
                match_val = int(input("Match: "))
                missmatch_val = int(input("Missmatch: "))
                gap_val = int(input("Gap: "))
                data = smith_waterman(sequence1, sequence2, data_type,match_val,missmatch_val,gap_val,input_type1,input_type2)
                save = str(input("Would you like to save data to file? (Y/n)"))
                if save == "Y":
                    jpg_name = str(input("Jpg file name: "))
                    seq_1_title = str(input("Sequence 1 title:"))
                    seq_2_title = str(input("Sequence 2 title:"))
                    save_plot(create_heatmap(data,seq_1_title,seq_2_title,title="Smith-Waterman"),jpg_name+".jpg")
                    txt_file = input("Text file name: ")
                    save_data_to_file(data,txt_file+".txt")
                    break
                elif save == "n":
                    break
                elif save not in ("Y","n"):
                    print("I didn't catch that.")
                    continue
            elif gmm == "n":
                match_val = 1
                missmatch_val = -2
                gap_val = -1
                data = smith_waterman(sequence1, sequence2, data_type, match_val, missmatch_val, gap_val,
                                          input_type1, input_type2)
                save = str(input("Would you like to save data to file? (Y/n)"))
                if save == "Y":
                    jpg_name = str(input("Jpg file name: "))
                    seq_1_title = str(input("Sequence 1 title:"))
                    seq_2_title = str(input("Sequence 2 title:"))
                    save_plot(create_heatmap(data, seq_1_title, seq_2_title,title="Smith-Waterman"), jpg_name + ".jpg")
                    txt_file = input("Text file name: ")
                    save_data_to_file(data, txt_file + ".txt")
                    break
                elif save == "n":
                    break
                elif save not in ("Y", "n"):
                    print("I didn't catch that.")
                    continue
            elif gmm not in ("Y", "n"):
                print("I didn't catch that")
                continue
        elif method == "B":
            while True:
                gmm = str(
                    input("Current values of match/missmatch/gap are 1/-2/-1. Would you like to change it? (Y/n)"))
                if gmm == "Y":
                    print("Values should be integer. Missmatch and gap should be negative.")
                    match_val = int(input("Match: "))
                    missmatch_val = int(input("Missmatch: "))
                    gap_val = int(input("Gap: "))
                    data = Needelman_Wunsch(sequence1, sequence2, data_type, gap_val, match_val, missmatch_val,
                                          input_type1, input_type2)
                    save = str(input("Would you like to save data to file? (Y/n)"))
                    if save == "Y":
                        jpg_name = str(input("Jpg file name: "))
                        seq_1_title = str(input("Sequence 1 title:"))
                        seq_2_title = str(input("Sequence 2 title:"))
                        save_plot(create_heatmap(data, seq_1_title, seq_2_title,title="Needleman-Wunsch"), jpg_name + ".jpg")
                        txt_file = input("Text file name: ")
                        save_data_to_file(data, txt_file + ".txt")
                        break
                    elif save == "n":
                        break
                    elif save not in ("Y", "n"):
                        print("I didn't catch that.")
                        continue
                elif gmm == "n":
                    match_val = 1
                    missmatch_val = -2
                    gap_val = -1
                    data = Needelman_Wunsch(sequence1, sequence2, data_type,gap_val,match_val, missmatch_val,
                                          input_type1, input_type2)
                    save = str(input("Would you like to save data to file? (Y/n)"))
                    if save == "Y":
                        jpg_name = str(input("Jpg file name: "))
                        seq_1_title = str(input("Sequence 1 title:"))
                        seq_2_title = str(input("Sequence 2 title:"))
                        save_plot(create_heatmap(data, seq_1_title, seq_2_title, title="Needleman-Wunsch"), jpg_name + ".jpg")
                        txt_file = input("Text file name: ")
                        save_data_to_file(data, txt_file + ".txt")
                        break
                    elif save == "n":
                        break
                    elif save not in ("Y", "n"):
                        print("I didn't catch that.")
                        continue
                elif gmm not in ("Y", "n"):
                    print("I didn't catch that")
                    continue
                else:
                    break
                break
            break
        elif input_type not in ("A", "B"):
            print("Incorrect answer. Choose option A or B.")
            continue
        else:
            print("Incorrect data type.")
            continue
    print("Great job! See ya later aligator!")


interactive_program()
